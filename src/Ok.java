
import java.io.ByteArrayInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.zip.GZIPInputStream;
import javax.swing.JOptionPane;

public final class Ok {

    public String decompressGzipFile(String no, byte[] myByteArray) {
        try {
            GZIPInputStream gis = new GZIPInputStream(new ByteArrayInputStream(myByteArray));
            FileOutputStream fos = new FileOutputStream(no + ".mp3");
            byte[] buffer = new byte[1024];
            int len;
            while ((len = gis.read(buffer)) != -1) {
                fos.write(buffer, 0, len);
            }
        } catch (IOException e) {
            System.out.println(e);
        }
        return null;
    }

    public static void main(String a[]) throws IOException {
        String no = JOptionPane.showInputDialog("Ingrese nombre de archivo sin etensión");
        Path path = Paths.get(no + ".gz");
        new Ok().decompressGzipFile(no, Files.readAllBytes(path));
    }

}
